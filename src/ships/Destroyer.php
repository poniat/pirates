<?php
declare(strict_types=1);

namespace Src\Ships;

use Src\Sailor;

class Destroyer extends Ship implements ShipInterface
{
    protected $attackPoints = 20;

    protected $defencePoints = 45;

    public $sailors;

    /**
     * Add a single sailor to Destroyer
     *
     * @param Sailor $sailor instance of the sailor
     * @return ShipInterface
     */
    public function addSailor(Sailor $sailor): ShipInterface
    {
        $this->health += ($sailor->getHealth() - 2);
        $this->attackPoints += ($sailor->getAttackPoints() * 2);
        $this->defencePoints += ($sailor->getDefencePoints() - 1);

        $this->sailors++;

        return $this;
    }


}