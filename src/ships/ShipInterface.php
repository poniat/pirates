<?php
declare(strict_types=1);

namespace Src\Ships;

use Src\Sailor;

interface ShipInterface
{
    public function addSailor(Sailor $sailor): self;


}