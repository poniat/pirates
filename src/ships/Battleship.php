<?php
declare(strict_types=1);

namespace Src\Ships;

use Src\Sailor;

class Battleship extends Ship implements ShipInterface
{
    protected $attackPoints = 35;

    protected $defencePoints = 10;

    /**
     * Add a single sailor to Battleship
     *
     * @param Sailor $sailor instance of the sailor
     * @return ShipInterface
     */
    public function addSailor(Sailor $sailor): ShipInterface
    {
        $this->health += ($sailor->getHealth() + 5);
        $this->attackPoints += $sailor->getAttackPoints();
        $this->defencePoints += ($sailor->getDefencePoints() + 1);

        return $this;
    }

}