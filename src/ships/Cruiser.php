<?php
declare(strict_types=1);

namespace Src\Ships;

use Src\Sailor;

class Cruiser extends Ship implements ShipInterface
{
    protected $attackPoints = 5;

    protected $defencePoints = 5;

    /**
     * Add a single sailor to Battleship
     *
     * @param Sailor $sailor instance of the sailor
     * @return ShipInterface
     */
    public function addSailor(Sailor $sailor): ShipInterface
    {
        $this->health += $sailor->getHealth();
        $this->attackPoints += $sailor->getAttackPoints();
        $this->defencePoints += $sailor->getDefencePoints();

        return $this;
    }


}