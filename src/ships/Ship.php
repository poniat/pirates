<?php
declare(strict_types=1);

namespace Src\Ships;

use Src\Sailor;

/**
 * Class Ship
 *
 * List of the ships: @doc http://wiki.wargaming.net/en/World_of_Warships
 * @package Src\Ships
 */
abstract class Ship
{
    protected $missingAttack = 25;

    protected $luckyShot = 10;

    protected $health = 100;

    protected $attackPoints = 15;

    protected $defencePoints = 5;

    public $sailors;

    /**
     * Calculate strong of the single ship's shot
     *
     * @param int $percent random percent
     * @return bool true = if is lucky shot or missed, false = nothing happened
     */
    public function calculateStrongOfShot(int $percent): bool
    {
        $count = (int)round((100 / $percent) - 1);
        $missed = array_merge([1], array_fill(1, $count, 0));

        // mt_rand(1, $count)
        return (bool) $missed[array_rand($missed)];
    }

    /**
     * Set ship health
     *
     * @param int $health level of health
     * @return Ship
     */
    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get level of ship health
     *
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * Add single sailor on the sip deck
     *
     * @param Sailor $sailor instance of the sailor
     * @return ShipInterface
     */
    public function addSailor(Sailor $sailor): ShipInterface
    {
        $this->health += $sailor->getHealth();
        $this->attackPoints += $sailor->getAttackPoints();
        $this->defencePoints += $sailor->getDefencePoints();

        return $this;
    }

    /**
     * Calculate possibility of missed shot
     *
     * @return bool true = missed, false - hit
     */
    public function missedShot(): bool
    {
        return $this->calculateStrongOfShot($this->missingAttack);
    }

    /**
     * Calculate possibility of the 'lucky shot"
     * @return bool true = lucky shot, false - nothing happened
     */
    public function luckyShot(): bool
    {
        return $this->calculateStrongOfShot($this->luckyShot);
    }

    /**
     * Calculate strong of the single shot
     *
     * @return array return an array with number
     * of health and defend witch will be taken
     */
    public function shot(): array
    {
        $missed = $this->missedShot();
        $luckyShot = $this->luckyShot();

        if($missed) {
            $health = 0;
            $defend = 0;
        }
        elseif($luckyShot) {
            $health = ($this->attackPoints * 3);
            $defend = 1;
        }
        else {
            $health = $this->attackPoints;
            $defend = 1;
        }

        return [
            'takeHealth' => $health,
            'takeDefend' => $defend
        ];
    }

    /**
     * Perform an attack
     *
     * @param Ship $ship instance of the defender ship
     * @return Ship instance of the attacker ship
     */
    public function attack(Ship $ship): Ship
    {
        $shot = $this->shot();
        $ship->defend($shot);

        return $this;
    }

    /**
     * When a ship is attacking another ship,
     * that ship can defend himself
     *
     * @param array $shot calculated strong of the shot
     * @return int return health level after a shot
     */
    public function defend(array $shot): int
    {
        if($this->defencePoints > 0) {
            $this->defencePoints -= $shot['takeDefend'];
            $this->health -= $shot['takeHealth'];
        }
        else {
            $this->health -= ($shot['takeHealth'] * 2);
        }

        return $this->health;
    }


}