<?php
declare(strict_types=1);

namespace Src;

class Sailor
{
    protected $health = 10;

    protected $attackPoints = 2;

    protected $defencePoints = 1;

    public function __construct(){}

    /**
     * Set sailor health
     *
     * @param int $health level of health
     * @return Sailor
     */
    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Set sailor strong of the attack
     *
     * @param int $attack level of the attack
     * @return Sailor
     */
    public function setAttackPoints(int $attack): self
    {
        $this->attackPoints = $attack;

        return $this;
    }

    /**
     * Set sailor level of defence
     * @param int $defend level of defence point
     * @return Sailor
     */
    public function setDefencePoints(int $defend): self
    {
        $this->defencePoints = $defend;

        return $this;
    }

    /**
     * Get sailor health level
     *
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * Get sailor attack point level
     * @return int
     */
    public function getAttackPoints(): int
    {
        return $this->attackPoints;
    }

    /**
     * Get sailor defence point level
     *
     * @return int
     */
    public function getDefencePoints(): int
    {
        return $this->defencePoints;
    }


}