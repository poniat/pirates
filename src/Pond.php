<?php
declare(strict_types=1);

namespace Src;

use Src\Ships\Ship;

class Pond
{
    private $ships = [];

    public $won;

    public $lost;

    /**
     * TODO - feature update
     * Run fights for multiple teams
     *
     * @param null|Ship $ship instance of the type of ship
     * @param string $teamName team name
     * @return array return array with results
     */
    public function addShip(?Ship $ship, string $teamName): array
    {
        return $this->ships[$teamName] = $ship;
    }

    /**
     * Run single attack
     * @param $shipA
     * @param $shipB
     * @return bool
     */
    public function singleAttack($shipA, $shipB)
    {
        $sunk = false;
        $shipA->attack($shipB);

        if($shipB->getHealth() < 1) {
            $sunk = true;
            $this->won = $shipA;
            $this->lost = $shipB;
        }
       else {
           $this->singleAttack($shipB, $shipA);
       }

        return $sunk;
    }

    /**
     * Run single fight between two ships
     *
     * @param null|Ship $shipA instance of the specific ship
     * @param null|Ship $shipB instance of the specific ship
     * @return array return array with results of the fight
     */
    public function fight(?Ship $shipA, ?Ship $shipB): Ship
    {
        do {
            $sunk = $this->singleAttack($shipA, $shipB);
        } while($sunk);

         return $this->won;
    }

    /**
     * Check if call is coming from CMD or browser
     *
     * @return bool return true if call is CMD, otherwise false
     */
    public function isCommandLineInterface() : bool
    {
        return (php_sapi_name() === 'cli');
    }

    /**
     * Make text bold in CMD
     *
     * @param string $text
     * @return string
     */
    public function getCLIBold(string $text) : string
    {
        return " \033[1m{$text}\033[0m ";
    }


}