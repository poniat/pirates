## Battle ship game
 
Lib based on PHP in version 7.1.

At this moment game provides  battle only between two ships.
 
## Features
Extend a game to multiple team of the ships fights.

####Project provides:
- Class which give you an simple example battle of ships.
- CMD output of a random fight result.
- PHPDoc.
- PHPUnit tests.
**Document checking completed. No errors or warnings to show.**

#### An installation guide.
1. Add PHP lib into your project and enjoy it.
2. Install a composer into main directory of the project. (`composer install` or `composer update`).
3. In order to test application run fallowing commands:
- `phpunit` - for phpunit test.
4. In order to run a demo fight in CMD, please go to /demo directory and run `php cmd.php`.

#### See working test below:
**[Test me live!](http://pirates.gcoders.co.uk/)**