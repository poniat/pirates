<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Src\Ships\Battleship;
use Src\Ships\Destroyer;
use Src\Pond;
use Src\Sailor;

$sailorA1 = new Sailor();
$sailorA1->setAttackPoints(10);
$sailorA1->setDefencePoints(1);
$sailorA1->getHealth(10);

$sailorB1 = new Sailor();
$sailorB1->getHealth(25);
$sailorB2 = new Sailor();

$shipA = new Destroyer();
$shipA->addSailor($sailorA1);

$shipB = new Battleship();
$shipB->addSailor($sailorB1);
$shipB->addSailor($sailorB2);


$pond = new Pond();
$result = $pond->fight($shipA, $shipB);


if($pond->isCommandLineInterface()) {
    echo 'Won: '  . $pond->getCLIBold((new \ReflectionClass($pond->won))->getShortName())  . ' HP: ' . $pond->won->getHealth() . PHP_EOL .
         'Lost: ' . $pond->getCLIBold((new \ReflectionClass($pond->lost))->getShortName()) . ' HP: ' . $pond->lost->getHealth();
}
