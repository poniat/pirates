<?php

use Src\Ships\Battleship;
use Src\Ships\Destroyer;
use Src\Sailor;

class DestroyerTest extends \PHPUnit_Framework_TestCase
{
    public function test_calculation_of_shot(): void
    {
        $ship = new Destroyer();
        $result = $ship->calculateStrongOfShot(3);

        $this->assertInternalType('bool', $result);
    }

    public function test_get_current_health_level(): void
    {
        $ship = new Destroyer();

        $this->assertInstanceOf('Src\Ships\Destroyer', $ship->setHealth(200));
        $this->assertEquals(200, $ship->getHealth());
    }

    public function test_adding_sailor_on_the_deck(): void
    {
        $ship = new Destroyer;
        $sailor = new Sailor();
        for ($i=0; $i<5; $i++) {
            $ship->addSailor($sailor);
        }

        $this->assertEquals(5, $ship->sailors);
    }

    public function test_accuracy_of_shot(): void
    {
        $shipA = new Destroyer();
        $result = $shipA->shot();

        $this->assertEquals(2, count($result));
        $this->assertInternalType('int', $result['takeHealth']);
        $this->assertInternalType('int', $result['takeDefend']);
    }

    public function test_of_the_single_attack(): void
    {
        $shipA = new Destroyer();
        $shipB = new Battleship();

        $this->assertInstanceOf('Src\Ships\Destroyer', $shipA->attack($shipB));
    }

    public function test_defend(): void
    {
        $ship = new Destroyer();
        $shot = $ship->shot();
        $result = $ship->defend($shot);

        $this->assertInternalType('int', $result);
        $this->assertTrue($ship->getHealth()<= $result);
    }


}