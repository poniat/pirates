<?php

use Src\Sailor;

class SailorTest extends \PHPUnit_Framework_TestCase
{
    public function test_sailor_setters(): void
    {
        $attack = 10;
        $defence = 20;
        $health = 30;

        $sailor = new Sailor;
        $setter1 = $sailor->setAttackPoints($attack);
        $setter2 = $sailor->setDefencePoints($defence);
        $setter3 = $sailor->setHealth($health);

        $instance = 'Src\Sailor';

        $this->assertInstanceOf($instance, $setter1);
        $this->assertInstanceOf($instance, $setter2);
        $this->assertInstanceOf($instance, $setter3);
    }

    public function test_sailor_getters(): void
    {
        $attack = 10;
        $defence = 20;
        $health = 30;

        $sailor = new Sailor;
        $sailor->setAttackPoints($attack);
        $sailor->setDefencePoints($defence);
        $sailor->setHealth($health);

        $this->assertEquals($attack, $sailor->getAttackPoints());
        $this->assertEquals($defence, $sailor->getDefencePoints());
        $this->assertEquals($health, $sailor->getHealth());
    }

    public function test_sailor_failed(): void
    {
        $sailor = new Sailor;
        $sailor->setHealth(-10);

        $this->assertNotEquals('Src\Ships\Destroyer', $sailor);
        $this->assertEquals(-10, $sailor->getHealth());
    }


}