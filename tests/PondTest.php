<?php

use Src\Ships\Battleship;
use Src\Ships\Destroyer;
use Src\Pond;

class PondTest extends \PHPUnit_Framework_TestCase
{
    public function test_fight_of_two_ships(): void
    {
        $shipA = new Destroyer();
        $shipB = new Battleship();
        $pond = new Pond();
        $result = $pond->fight($shipA, $shipB);

        $this->assertInternalType('object', $result);
    }

    public function test_if_stronger_ship_can_won_single_attack()
    {
        $shipA = new Destroyer();
        $shipA->setHealth(10);
        $shipB = new Battleship();

        $pond = new Pond();
        $pond->fight($shipA, $shipB);

        $this->assertInstanceOf('Src\Ships\Battleship', $pond->won);
    }

    public function test_has_returns_true_if_script_is_run_via_cmd(): void
    {
        $pond = new Pond;
        $cmd = $pond->isCommandLineInterface();

        $this->assertTrue($cmd);
    }


}